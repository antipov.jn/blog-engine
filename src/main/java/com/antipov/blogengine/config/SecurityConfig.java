package com.antipov.blogengine.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    public SecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                .antMatchers("/blog/article/create").hasRole("ADMIN")
                .antMatchers("/blog/article/{id:\\d+}/update").hasRole("ADMIN")
                .antMatchers("/blog/**").permitAll()

                .antMatchers(HttpMethod.POST,"/blog-engine/article/{id:\\d+}/comment").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.DELETE,"/blog-engine/article/{id:\\d+}/comment").hasRole("ADMIN")
                .antMatchers("/blog-engine/**").hasRole("ADMIN")

                .antMatchers("/login").anonymous()
                .and().formLogin().defaultSuccessUrl("/blog")
                .and().csrf().disable();
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
