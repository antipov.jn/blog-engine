package com.antipov.blogengine.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDtoException extends RuntimeException {

    public InvalidDtoException(final String message) {
        super(message);
    }
}
