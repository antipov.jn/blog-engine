package com.antipov.blogengine.entity;

import com.antipov.blogengine.dto.CommentDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Accessors(chain = true)
public class Comment {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String author;
    private String content;
    @ManyToOne
    private Article article;

    public Comment(final String author, final String content, final Article article) {
        this.author = author;
        this.content = content;
        this.article = article;
    }

    public Comment(final CommentDto commentDto, final Article article){
        this.author = commentDto.getAuthor();
        this.content = commentDto.getContent();
        this.article = article;
    }

    public CommentDto toDto(){
        return new CommentDto(id, author, content, article.getId());
    }
}
