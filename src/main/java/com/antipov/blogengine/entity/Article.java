package com.antipov.blogengine.entity;

import com.antipov.blogengine.dto.ArticleDto;
import com.antipov.blogengine.dto.CommentDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Accessors(chain = true)
public class Article {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String header;
    private String content;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "article_id")
    private List<Comment> comments;

    public Article(final String header, final String content) {
        this.header = header;
        this.content = content;
        this.comments = new ArrayList<>();
    }

    public Article(final String header, final String content, final List<Comment> comments) {
        this.header = header;
        this.content = content;
        this.comments = comments;
    }

    public Article(final ArticleDto articleDto){
        this.id = articleDto.getId();
        this.header = articleDto.getHeader();
        this.content = articleDto.getContent();
        this.comments = new ArrayList<>();
    }

    public ArticleDto toDto(){
        final List<CommentDto> commentsDto = comments.stream()
                .map(Comment::toDto)
                .collect(Collectors.toUnmodifiableList());
        return new ArticleDto(id, header, content, commentsDto);
    }
}
