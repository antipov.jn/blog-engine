package com.antipov.blogengine.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class User {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_role", joinColumns = @JoinColumn( name = "user_id"),
                            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;

    public User(final String username, final String password, final List<Role> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
}
