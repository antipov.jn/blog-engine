package com.antipov.blogengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class BlogEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogEngineApplication.class, args);
	}

}
