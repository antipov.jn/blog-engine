package com.antipov.blogengine.application;

import com.antipov.blogengine.dto.ArticleDto;
import com.antipov.blogengine.dto.CommentDto;
import com.antipov.blogengine.entity.Article;
import com.antipov.blogengine.entity.Comment;
import com.antipov.blogengine.exception.InvalidDtoException;
import com.antipov.blogengine.repository.ArticleRepository;
import com.antipov.blogengine.repository.CommentRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class BlogContentManagerUcs {

    private static int DEFAULT_ARTICLES_PER_PAGE = 10;

    private final ArticleRepository articleRepository;
    private final CommentRepository commentRepository;

    public BlogContentManagerUcs(final ArticleRepository articleRepository,
                                 final CommentRepository commentRepository) {
        this.articleRepository = articleRepository;
        this.commentRepository = commentRepository;
    }
    @Transactional
    public List<ArticleDto> getArticles(final int page) {
        return getArticles(page, DEFAULT_ARTICLES_PER_PAGE);
    }

    @Transactional(readOnly = true)
    public List<ArticleDto> getArticles(final int page, final int articlePerPage) {
        return articleRepository.findAll(PageRequest.of(page, articlePerPage))
                .stream()
                .map(Article::toDto)
                .collect(Collectors.toUnmodifiableList());
    }

    @Transactional(readOnly = true)
    public ArticleDto getArticle(final Long id) {
        return getArticleById(id).toDto();
    }

    @Transactional
    public ArticleDto saveArticle(final ArticleDto articleDto) {
        verifyArticleDtoForSave(articleDto);
        final Article article = new Article(articleDto);
        return articleRepository.save(article).toDto();
    }

    @Transactional
    public ArticleDto updateArticle(final ArticleDto articleDto) {
        verifyArticleIdForUpdateOrDelete(articleDto.getId());
        final Article article = articleRepository.findById(articleDto.getId())
                .orElseThrow(getEntityNotFoundExceptionSupplier(Article.class, articleDto.getId()));
        article.setHeader(articleDto.getHeader());
        article.setContent(articleDto.getContent());
        return articleRepository.save(article).toDto();
    }

    @Transactional
    public void deleteArticle(final Long id) {
        verifyArticleIdForUpdateOrDelete(id);
        articleRepository.deleteById(id);
    }

    @Transactional
    public CommentDto saveComment(final CommentDto commentDto) {
        final Long articleId = verifyCommentDtoForSave(commentDto);
        final Article relatedArticle = articleRepository.findById(articleId)
                .orElseThrow(getEntityNotFoundExceptionSupplier(Article.class, articleId));
        final Comment savedComment = commentRepository.save(new Comment(commentDto, relatedArticle));
        return savedComment.toDto();
    }

    @Transactional
    public void deleteComment(final Long articleId, final Long commentId){
        final Article article = getArticleById(articleId);
        final Comment comment = article.getComments().stream()
                .filter(c -> c.getId().equals(commentId))
                .findFirst()
                .orElseThrow(getEntityNotFoundExceptionSupplier(Comment.class, commentId));
        article.getComments().remove(comment);
        articleRepository.save(article);
        commentRepository.delete(comment);
    }

    private Article getArticleById(final Long id){
            return articleRepository.findById(id)
                    .orElseThrow(getEntityNotFoundExceptionSupplier(Article.class, id));
    }

    private Long verifyCommentDtoForSave(final CommentDto commentDto) {
        if (commentDto.getArticleId() == null)
            throw new InvalidDtoException(format("%s hasn't articleId ", commentDto.getClass().toString()));

        if (commentDto.getId() != null)
            throw new InvalidDtoException(format("%s has id while save's", commentDto.getClass().toString()));

        return commentDto.getArticleId();

    }

    private void verifyArticleIdForUpdateOrDelete(final Long id) {
        if (id == null)
            throw new InvalidDtoException(format("%s hasn't id", ArticleDto.class));
    }

    private void verifyArticleDtoForSave(final ArticleDto articleDto) {
        if (articleDto.getId() != null)
            throw new InvalidDtoException(format("%s has id while save's", articleDto.getClass().toString()));
    }

    private Supplier<EntityNotFoundException> getEntityNotFoundExceptionSupplier(final Class<?> clazz, final Long id) {
        return () -> new EntityNotFoundException(format("%s with id:%d was not found", clazz.toString(), id));
    }
}
