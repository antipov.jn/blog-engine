package com.antipov.blogengine.controller;

import com.antipov.blogengine.application.BlogContentManagerUcs;
import com.antipov.blogengine.dto.ArticleDto;
import com.antipov.blogengine.dto.CommentDto;
import com.antipov.blogengine.exception.InvalidDtoException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Controller
@RequestMapping("/blog-engine")
public class BlogEngineController {

    private final BlogContentManagerUcs blogContentManagerUcs;

    public BlogEngineController(final BlogContentManagerUcs blogContentManagerUcs) {
        this.blogContentManagerUcs = blogContentManagerUcs;
    }

    @PostMapping("/article")
    public ModelAndView saveArticle(@Valid @ModelAttribute final ArticleDto articleDto,
                                    final BindingResult bindingResult,
                                    final RedirectAttributes model) {
        if (bindingResult.hasErrors()) {
            model.addFlashAttribute("errorMessages", mapValidationErrorsToMessages(bindingResult));
            return new ModelAndView("redirect:/blog/article/create", model.getFlashAttributes());
        }
        blogContentManagerUcs.saveArticle(articleDto);

        return new ModelAndView("redirect:/blog");
    }

    @PutMapping("/article/{articleId}")
    public ModelAndView updateArticle(@PathVariable final Long articleId,
                                      @Valid @ModelAttribute final ArticleDto articleDto,
                                      final BindingResult bindingResult,
                                      final RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errorMessages", mapValidationErrorsToMessages(bindingResult));
            return new ModelAndView(format("redirect:/blog/article/%d/update", articleId), redirectAttributes.getFlashAttributes());
        }

        verifyArticleDtoId(articleDto, articleId);
        blogContentManagerUcs.updateArticle(articleDto);

        return new ModelAndView("redirect:/blog");
    }

    @DeleteMapping("/article/{id}")
    public String deleteArticle(@PathVariable final Long id) {
        blogContentManagerUcs.deleteArticle(id);
        return "redirect:/blog";
    }

    @PostMapping("/article/{id}/comment")
    public ModelAndView saveComment(@PathVariable final Long id,
                                    @Valid final CommentDto commentDto,
                                    final BindingResult bindingResult,
                                    final RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errorMessages", mapValidationErrorsToMessages(bindingResult));
            return new ModelAndView(format("redirect:/blog/article/%d", id), redirectAttributes.getFlashAttributes());
        }
        verifyCommentDtoArticleId(commentDto, id);
        blogContentManagerUcs.saveComment(commentDto);

        return new ModelAndView(format("redirect:/blog/article/%d", id));
    }

    @DeleteMapping("/article/{articleId}/comment/{id}")
    public String deleteComment(@PathVariable final Long articleId ,@PathVariable final Long id) {
        blogContentManagerUcs.deleteComment(articleId, id);
        return format("redirect:/blog/article/%d", articleId);
    }

    private List<String> mapValidationErrorsToMessages(final BindingResult bindingResult) {
        return bindingResult.getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toUnmodifiableList());
    }

    private void verifyCommentDtoArticleId(final CommentDto commentDto, final Long id) {
        if (!commentDto.getArticleId().equals(id))
            throw new InvalidDtoException(format("Id for %s not match", commentDto.getClass()));
    }

    private void verifyArticleDtoId(final ArticleDto articleDto, final Long id) {
        if (!articleDto.getId().equals(id))
            throw new InvalidDtoException(format("Id for %s not match", articleDto.getClass()));
    }

}
