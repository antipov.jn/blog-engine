package com.antipov.blogengine.controller;

import com.antipov.blogengine.application.BlogContentManagerUcs;
import com.antipov.blogengine.dto.ArticleDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/blog")
public class BlogViewController {

    private final BlogContentManagerUcs blogContentManagerUcs;

    public BlogViewController(final BlogContentManagerUcs blogContentManagerUcs) {
        this.blogContentManagerUcs = blogContentManagerUcs;
    }

    @GetMapping
    public String getAllArticles(@RequestParam(name = "p", required = false) final String pageStr,
                                 final Authentication authentication, final Model model) {
        final boolean isAdmin = hasAdminRole(authentication);
        final int page = parsePageParam(pageStr);
        final List<ArticleDto> articles = blogContentManagerUcs.getArticles(page);

        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("articleList", articles);

        return "all_articles";
    }

    @GetMapping("/article/{id}")
    public String getArticle(@PathVariable final Long id,  final Authentication authentication, final Model model) {
        model.addAttribute("isAdmin", hasAdminRole(authentication));
        model.addAttribute("username", getUsername(authentication)) ;
        model.addAttribute("article", blogContentManagerUcs.getArticle(id));

        return "article";
    }

    private String getUsername(Authentication authentication) {
        return (authentication != null) ?((UserDetails) authentication.getPrincipal()).getUsername() : null;
    }

    @GetMapping("/article/create")
    public String getCreateArticleForm(final ModelMap model) {
        model.addAttribute("method", "post");
        model.addAttribute("submitValue", "create");
        return "create_or_update_article";
    }

    @GetMapping("/article/{id}/update")
    public String getUpdateArticleForm(@PathVariable final Long id, final ModelMap model) {
        model.addAttribute("article", blogContentManagerUcs.getArticle(id));
        model.addAttribute("method", "put");
        model.addAttribute("submitValue", "update");

        return "create_or_update_article";
    }

    private int parsePageParam(final String pageStr) {
        return StringUtils.isNumeric(pageStr) ? Integer.parseInt(pageStr) : 0;
    }

    private boolean hasAdminRole(Authentication authentication) {
        return authentication != null && authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
    }
}
