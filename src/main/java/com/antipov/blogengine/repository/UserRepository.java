package com.antipov.blogengine.repository;

import com.antipov.blogengine.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findUserByUsername(final String username);
}
