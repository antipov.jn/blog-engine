package com.antipov.blogengine.repository;

import com.antipov.blogengine.entity.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
