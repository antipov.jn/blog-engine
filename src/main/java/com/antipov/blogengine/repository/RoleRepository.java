package com.antipov.blogengine.repository;

import com.antipov.blogengine.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
