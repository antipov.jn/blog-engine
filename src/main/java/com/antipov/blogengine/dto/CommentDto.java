package com.antipov.blogengine.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CommentDto {

    private final Long id;
    @NotBlank(message = "{commentdto.author.notblank}")
    private final String author;
    @NotBlank(message = "{commentdto.content.notblank}")
    private final String content;
    private final Long articleId;
}
