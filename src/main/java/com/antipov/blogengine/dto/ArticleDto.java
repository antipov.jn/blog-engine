package com.antipov.blogengine.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ArticleDto {

    private final Long id;
    @NotBlank(message = "{articledto.header.notblank}")
    private final String header;
    @NotBlank(message = "{articledto.content.notblank}")
    private final String content;
    private final List<CommentDto> comments;

}
