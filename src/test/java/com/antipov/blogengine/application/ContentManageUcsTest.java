package com.antipov.blogengine.application;

import com.antipov.blogengine.dto.ArticleDto;
import com.antipov.blogengine.dto.CommentDto;
import com.antipov.blogengine.exception.InvalidDtoException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ContentManageUcsTest {

    @Autowired
    private BlogContentManagerUcs contentManager;

    @Test
    public void testContentManagerCrudMethods() {
        assertThrows(EntityNotFoundException.class, () -> contentManager.getArticle(-1L));

        final ArticleDto articleDtoWithId = new ArticleDto(-1L, "header", "content", null);
        assertThrows(InvalidDtoException.class, () -> contentManager.saveArticle(articleDtoWithId));

        final ArticleDto articleDtoForSave = new ArticleDto(null, "header", "content", null);
        final ArticleDto savedArticleDto = contentManager.saveArticle(articleDtoForSave);
        assertNotNull(savedArticleDto.getId());

        final ArticleDto articleDtoForUpdate = new ArticleDto(savedArticleDto.getId(), "updated header", "updated content", savedArticleDto.getComments());
        final ArticleDto updatedArticleDto = contentManager.updateArticle(articleDtoForUpdate);

        assertTrue(updatedArticleDto.getId().equals(articleDtoForUpdate.getId())
                && updatedArticleDto.getContent().equals(articleDtoForUpdate.getContent())
                && updatedArticleDto.getHeader().equals(articleDtoForUpdate.getHeader())
                && updatedArticleDto.getComments().equals(articleDtoForUpdate.getComments()));


        final CommentDto commentDtoWithId = new CommentDto(100L, "author", "content", updatedArticleDto.getId());
        assertThrows(InvalidDtoException.class, () -> contentManager.saveComment(commentDtoWithId));

        final CommentDto commentDtoForNonExistentArticle = new CommentDto(null, "author", "content", -1L);
        assertThrows(EntityNotFoundException.class, () -> contentManager.saveComment(commentDtoForNonExistentArticle));

        final CommentDto commentDtoForSave = new CommentDto(null, "author", "content", updatedArticleDto.getId());
        final CommentDto savedCommentDto = contentManager.saveComment(commentDtoForSave);
        assertNotNull(savedCommentDto.getId());

        final ArticleDto articleDtoWithSavedComment = contentManager.getArticle(savedArticleDto.getId());
        assertEquals(articleDtoWithSavedComment.getComments().get(0), savedCommentDto);

        contentManager.deleteComment(savedCommentDto.getArticleId(), savedCommentDto.getId());
        final ArticleDto articleDtoAfterCommentWasDelete = contentManager.getArticle(savedArticleDto.getId());
        assertTrue(articleDtoAfterCommentWasDelete.getComments().isEmpty());


        final List<ArticleDto> firstSavedArticlesPage = contentManager.getArticles(0);
        final List<ArticleDto> secondSavedArticlesPage = contentManager.getArticles(1);
        assertEquals(1, firstSavedArticlesPage.size());
        assertTrue(secondSavedArticlesPage.isEmpty());


        contentManager.deleteArticle(savedArticleDto.getId());
        final List<ArticleDto> savedArticles = contentManager.getArticles(0);
        assertTrue(savedArticles.isEmpty());

    }
}
