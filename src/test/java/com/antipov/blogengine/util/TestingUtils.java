package com.antipov.blogengine.util;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class TestingUtils {

    private TestingUtils() { }

    public static String encodeBody(final Map<String, String> body){
        final StringBuilder sb = new StringBuilder();
        body.forEach((key, value) -> sb.append(URLEncoder.encode(key, StandardCharsets.UTF_8))
                .append("=")
                .append(URLEncoder.encode(value, StandardCharsets.UTF_8))
                .append("&"));

        return sb.toString();
    }
}
