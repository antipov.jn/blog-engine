package com.antipov.blogengine.controller;


import com.antipov.blogengine.dto.CommentDto;
import com.antipov.blogengine.entity.Article;
import com.antipov.blogengine.entity.Comment;
import com.antipov.blogengine.repository.ArticleRepository;
import com.antipov.blogengine.repository.CommentRepository;
import com.antipov.blogengine.util.TestingUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WithMockUser(username = "user", roles = "USER")
public class UserBlogControllersTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentRepository commentRepository;
    @MockBean
    private ArticleRepository articleRepository;

    private  final Article dummyArticle = new Article("header", "content").setId(1L);
    private final Comment dummyComment = new Comment("author", "content", dummyArticle).setId(1L);

    @Before
    public void mockArticleRepository(){
        Mockito.when(commentRepository.save(Mockito.any(Comment.class))).thenReturn(dummyComment);
        Mockito.when(articleRepository.findById(1L))
                .thenReturn(Optional.of(dummyArticle));

        final ArrayList<Comment> comments = new ArrayList<>();
        comments.add(dummyComment);
        dummyArticle.setComments(comments);
    }

    @Test
    public void testAccessDeniedForCreateArticleView() throws Exception {
        mockMvc.perform(get("/blog/article/create"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testAccessDeniedForUpdateArticleView() throws Exception {
        mockMvc.perform(get("/blog/article/create"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testAccessDeniedForCreateArticle() throws Exception {
        mockMvc.perform(post("/blog-engine/article/create").content("dummy"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testAccessDeniedForUpdateArticle() throws Exception {
        mockMvc.perform(put("/blog-engine/article/1/update").content("dummy"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testAccessDeniedForDeleteComment() throws Exception {
        mockMvc.perform(delete("/blog-engine/article/1/comment/1").content("dummy"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testSuccessSaveComment() throws Exception {
        final CommentDto commentDto = new CommentDto(null, "user", "content", 1L);
        final MockHttpServletRequestBuilder requestBuilder = post("/blog-engine/article/1/comment")
                .content(TestingUtils.encodeBody(convertCommentDtoToMap(commentDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);
        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.redirectedUrl("/blog/article/1"));
    }

    private Map<String, String> convertCommentDtoToMap(final CommentDto commentDto) {
        final Map<String, String> body = new HashMap<>();
        final String idStr = commentDto.getId() == null ? "" : String.valueOf(commentDto.getId());
        final String articleIdStr = commentDto.getArticleId() == null ? "" : String.valueOf(commentDto.getArticleId());

        body.put("id", idStr);
        body.put("articleId", articleIdStr);
        body.put("author", commentDto.getAuthor());
        body.put("content", commentDto.getContent());

        return body;
    }
}
