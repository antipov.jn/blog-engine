package com.antipov.blogengine.controller;

import com.antipov.blogengine.dto.ArticleDto;
import com.antipov.blogengine.entity.Article;
import com.antipov.blogengine.entity.Comment;
import com.antipov.blogengine.repository.ArticleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

import static com.antipov.blogengine.util.TestingUtils.encodeBody;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WithMockUser(username = "user", roles = "ADMIN")
public class AdminBlogControllersTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ArticleRepository articleRepository;

    private  final Article dummyArticle = new Article("header", "content").setId(1L);
    private final Comment dummyComment = new Comment("author", "content", dummyArticle).setId(1L);

    @Before
    public void mockArticleRepository(){
        Mockito.when(articleRepository.save(Mockito.any(Article.class))).thenReturn(dummyArticle);
        Mockito.mock(articleRepository.getClass()).deleteById(1L);
        Mockito.when(articleRepository.findById(1L))
                .thenReturn(Optional.of(dummyArticle));

        final ArrayList<Comment> comments = new ArrayList<>();
        comments.add(dummyComment);
        dummyArticle.setComments(comments);
    }

    @Test
    public void testAccessToCreateArticleView() throws Exception {
        mockMvc.perform(get("/blog/article/create"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testAccessToUpdateArticleView() throws Exception {
        mockMvc.perform(get("/blog/article/1/update"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testSuccessCreateArticle() throws Exception {
        final ArticleDto articleDto = new ArticleDto(null, "test header", "test content", Collections.emptyList());;
        final MockHttpServletRequestBuilder requestBuilder = post("/blog-engine/article")
                .content(encodeBody(convertArticleDtoToMap(articleDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.redirectedUrl("/blog"));
    }

    @Test
    public void testNotSuccessCreateArticleWithId() throws Exception {
        final ArticleDto articleDto = new ArticleDto(1L, "test header", "test content", Collections.emptyList());
        final MockHttpServletRequestBuilder requestBuilder = post("/blog-engine/article")
                .content(encodeBody(convertArticleDtoToMap(articleDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testNotSuccessCreateArticleWithValidationExceptions() throws Exception {
        final ArticleDto invalidArticleDto = new ArticleDto(1L, "", "", Collections.emptyList());
        final MockHttpServletRequestBuilder requestBuilder = post("/blog-engine/article")
                .content(encodeBody(convertArticleDtoToMap(invalidArticleDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.redirectedUrlPattern("/blog/article/create?{.+}"));
    }


    @Test
    public void testSuccessUpdateArticle() throws Exception {
        final ArticleDto articleDto = new ArticleDto(1L, "test header", "test content", Collections.emptyList());
        final MockHttpServletRequestBuilder requestBuilder = put("/blog-engine/article/1")
                .content(encodeBody(convertArticleDtoToMap(articleDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.redirectedUrl("/blog"));
    }

    @Test
    public void testNotSuccessUpdateArticleWithIncorrectIdInUrlPath() throws Exception {
        final ArticleDto articleDto = new ArticleDto(1L, "test header", "test content", Collections.emptyList());;
        final MockHttpServletRequestBuilder requestBuilder = put("/blog-engine/article/2")
                .content(encodeBody(convertArticleDtoToMap(articleDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testNotSuccessUpdateArticleWithValidationExceptions() throws Exception {
        final ArticleDto invalidArticleDto = new ArticleDto(1L, "", "", Collections.emptyList());
        final MockHttpServletRequestBuilder requestBuilder = put("/blog-engine/article/1")
                .content(encodeBody(convertArticleDtoToMap(invalidArticleDto)))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.redirectedUrlPattern("/blog/article/1/update?{.+}"));
    }

    @Test
    public void testSuccessDeleteArticle() throws Exception {
        final MockHttpServletRequestBuilder content = delete("/blog-engine/article/1");
        mockMvc.perform(content).andExpect(MockMvcResultMatchers.redirectedUrl("/blog"));
    }

    @Test
    public void testSuccessDeleteComment() throws Exception {
        final MockHttpServletRequestBuilder content = delete("/blog-engine/article/1/comment/1");
        mockMvc.perform(content).andExpect(MockMvcResultMatchers.redirectedUrl("/blog/article/1"));
    }


    private Map<String, String> convertArticleDtoToMap(final ArticleDto articleDto) {
        final Map<String, String> body = new HashMap<>();
        final String idStr = articleDto.getId() == null ? "" : String.valueOf(articleDto.getId());
        body.put("id", idStr);
        body.put("header", articleDto.getHeader());
        body.put("content", articleDto.getContent());

        return body;
    }

}
